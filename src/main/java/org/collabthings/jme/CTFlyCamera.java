package org.collabthings.jme;

import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;

public class CTFlyCamera implements CTCamera {

	private Camera cam;

	public CTFlyCamera(Camera cam) {
		this.cam = cam;
	}

	@Override
	public void setDistance(float cameradistance) {
		//
	}

	@Override
	public void setLocation(Vector3f clone) {
		cam.setLocation(clone);
	}

	@Override
	public void lookAt(Vector3f camlookat, Vector3f vector3f) {
		cam.lookAt(camlookat, vector3f);
	}

}
