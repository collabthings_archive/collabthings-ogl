package org.collabthings.jme;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.collabthings.core.utils.WTimedFlag;
import org.collabthings.math.TransformStack;
import org.collabthings.model.CTMaterial;
import org.collabthings.model.CTModel;
import org.collabthings.model.CTPart;
import org.collabthings.model.CTPartBuilder;
import org.collabthings.model.CTSubPart;
import org.collabthings.model.CTTriangle;
import org.collabthings.model.CTTriangleMesh;
import org.collabthings.model.impl.CTSubPartImpl;
import org.collabthings.util.LLog;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Matrix4f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;
import com.jme3.util.TangentBinormalGenerator;

public class CTObjectViewerImpl implements CTObjectViewer {
	private LLog log = LLog.getLogger(this);

	private GTViewProperties viewproperties = new GTViewProperties();

	private CTPart currentpart;
	private Map<String, Material> materials = new HashMap<>();
	private Map<CTSubPart, SubpartMeshes> subpartmeshes = new HashMap<>();

	private Node root;

	private boolean debugnormals = false;
	private float scale = 0.001f;

	private AssetManager assets;
	private final WTimedFlag checkflag = new WTimedFlag(300);

	private Thread processthread;

	private long lastupdate;

	private String processednodeid;

	public CTObjectViewerImpl(AssetManager assets) {
		this.assets = assets;
		LLog.getLogger(this).info("JMEScene");
	}

	@Override
	public void setScale(float f) {
		scale = f;
	}

	@Override
	public float getScale() {
		return scale;
	}

	@Override
	public void update(Node root) {
		lastupdate = System.currentTimeMillis();
		if (currentpart != null) {
			this.root = root;
			root.setLocalScale(scale);

			if (checkflag.isTriggered()) {
				List<CTSubPart> all = new LinkedList<>();
				all.addAll(subpartmeshes.keySet());
				List<Node> found = new ArrayList<Node>();

				// removing nodes that are attached to root, but not found in
				// subpartmeshes
				removeNotFoundMeshes(root, found);

				// attaching geometries that are in subpartmeshes, but not found
				// in root
				attachNewMeshes(root, found);

				checkflag.reset();
			}
		}
	}

	private void removeNotFoundMeshes(Node root, List<Node> found) {
		root.getChildren().stream().forEach(child -> {
			new HashSet<>(subpartmeshes.keySet()).stream().forEach(subpart -> {
				SubpartMeshes m = subpartmeshes.get(subpart);
				if (m != null && m.n != null && m.n.getName().equals(getNodeID(subpart))) {
					found.add(m.n);
				}
			});

			if (!found.contains(child)) {
				child.removeFromParent();
				new HashSet<>(subpartmeshes.keySet()).forEach(subpart -> {
					SubpartMeshes mesh = subpartmeshes.get(subpart);
					if (mesh.n == child) {
						subpartmeshes.remove(subpart);
					}
				});
			}
		});
	}

	private void attachNewMeshes(Node root, List<Node> found) {
		new HashSet<>(subpartmeshes.keySet()).stream().forEach(subpart -> {
			SubpartMeshes m = subpartmeshes.get(subpart);
			if (m != null) {
				if (m.n == null || !m.n.getName().equals(getNodeID(subpart))) {
					log.info("new Node with " + subpart + " part:" + subpart.getPart());
					log.info("Node count " + root.getChildren().size() + " vertex count:" + root.getVertexCount()
							+ " trianglecount:" + root.getTriangleCount());
					m.n = new Node();
					m.n.setName(getNodeID(subpart));

					m.meshes.keySet().forEach(material -> {
						Buffers buffers = m.meshes.get(material);
						m.n.attachChild(buffers.g);

						if (debugnormals) {
							Geometry debug = new Geometry("Debug normals",
									TangentBinormalGenerator.genTbnLines(buffers.g.getMesh(), 100000.08f));
							Material debugMat = getAssetManager().loadMaterial("Common/Materials/VertexColor.j3m");
							debug.setMaterial(debugMat);
							debug.setCullHint(Spatial.CullHint.Never);
							// debug.getLocalTranslation().set(translation);
							m.n.attachChild(debug);
						}
					});
				}

				if (!found.contains(m.n)) {
					root.attachChild(m.n);
				}

				m.n.setLocalTransform(subpart.getTransformation());
			} else {
				subpartmeshes.remove(subpart);
			}
		});
	}

	private String getNodeID(CTPart part) {
		StringBuilder sb = new StringBuilder();

		part.getSubParts().stream().forEach(subpart -> {
			sb.append(getNodeID(subpart));
		});

		if (part.getModel() != null) {
			sb.append(getNodeID(part.getModel()));
		}

		if (part.getBuilder() != null) {
			sb.append(getNodeID(part.getBuilder()));

		}
		return sb.toString();
	}

	private String getNodeID(CTPartBuilder builder) {
		StringBuilder sb = new StringBuilder();
		sb.append("s:" + builder.getApplication().getID().hashCode());
		sb.append(builder.isReady());
		return sb.toString();
	}

	private String getNodeID(CTModel model) {
		StringBuilder sb = new StringBuilder();
		sb.append("m:" + model.getModified());
		sb.append("-" + model.isReady());
		return sb.toString();
	}

	private String getNodeID(CTSubPart subpart) {
		StringBuilder sb = new StringBuilder();
		CTPart p = subpart.getPart();
		if (p.getModel() != null) {
			sb.append("" + p.getModel().isReady() + p.getModel().getModified());
		} else {
			for (CTSubPart subsub : p.getSubParts()) {
				sb.append(getNodeID(subsub));
			}
		}
		return sb.toString();
	}

	private AssetManager getAssetManager() {
		return assets;
	}

	private SubpartMeshes addSubPart(Map<CTSubPart, SubpartMeshes> subpartmeshes, CTSubPart subpart) {
		log.info("Adding subpart " + subpart);

		SubpartMeshes subpartmesh;
		subpartmesh = new SubpartMeshes();
		subpartmeshes.put(subpart, subpartmesh);

		TransformStack stack = new TransformStack();
		// stack.push(new Transform(new Vector3f(), new Quaternion(), new
		// Vector3f(scale, scale, scale)));

		updateMeshes(stack, subpart.getPart(), subpartmesh);

		final Map<Material, Buffers> spmeshes = subpartmesh.meshes;
		spmeshes.keySet().stream().forEach((material) -> {
			Buffers buffers = spmeshes.get(material);

			Mesh m = new Mesh();
			Vector3f[] avs = new Vector3f[buffers.vs.size()];
			buffers.vs.toArray(avs);
			m.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(avs));

			int[] atris = new int[buffers.tris.size()];
			for (int i = 0; i < atris.length; i += 3) {
				atris[i + 0] = buffers.tris.get(i + 0);
				atris[i + 2] = buffers.tris.get(i + 1);
				atris[i + 1] = buffers.tris.get(i + 2);
			}

			m.setBuffer(Type.Index, 3, atris);

			Vector3f[] ans = new Vector3f[buffers.ns.size()];
			buffers.ns.toArray(ans);
			m.setBuffer(Type.Normal, 3, BufferUtils.createFloatBuffer(ans));

			m.updateBound();

			Geometry g = new Geometry();
			g.setMaterial(material);
			g.setMesh(m);

			buffers.vs = null;
			buffers.ns = null;
			buffers.tris = null;
			buffers.g = g;
		});

		return subpartmesh;
	}

	private void updateMeshes(TransformStack stack, CTPart part, SubpartMeshes subpartmesh) {
		if (part != null && part.isReady()) {
			if (part.getModel() != null && part.getModel().isReady()) {
				addModel(stack.current(), part, subpartmesh);
			} else {
				part.getSubParts().stream().forEach(subpart -> {
					stack.push(subpart.getTransformation());
					updateMeshes(stack, subpart.getPart(), subpartmesh);
					stack.pop();
				});
			}
		}
	}

	private synchronized void addModel(Matrix4f matrix4f, CTPart p, SubpartMeshes meshes) {
		Material material = getMaterial(p.getMaterial());
		Buffers buffers = meshes.meshes.get(material);
		if (buffers == null) {
			buffers = new Buffers();
			meshes.meshes.put(material, buffers);
		}

		CTTriangleMesh tmesh = p.getModel().getTriangleMesh();

		int itriangle = buffers.vs.size();

		Vector3f temp = new Vector3f();
		List<Vector3f> vectors = tmesh.getVectors();
		for (int iv = 0; iv < vectors.size(); iv++) {
			Vector3f v = vectors.get(iv);
			v = matrix4f.mult(v, temp);
			buffers.vs.add(toJMEVector(temp));
		}

		List<CTTriangle> triangles = tmesh.getTriangles();

		while (buffers.ns.size() < buffers.vs.size()) {
			buffers.ns.add(new Vector3f());
		}

		for (int it = 0; it < triangles.size(); it++) {
			CTTriangle t = triangles.get(it);
			int ia = t.getA() + itriangle;
			buffers.tris.add(ia);
			int ib = t.getB() + itriangle;
			buffers.tris.add(ib);
			int ic = t.getC() + itriangle;
			buffers.tris.add(ic);

			Vector3f n = t.getN();
			if (n == null) {
				// matrix4f.multNormalAcross(n, temp);
				Vector3f ba = buffers.vs.get(ia).subtract(buffers.vs.get(ib));
				Vector3f bc = buffers.vs.get(ic).subtract(buffers.vs.get(ib));

				Vector3f nn = bc.cross(ba).normalize().mult(-1);
				// float dot = n.dot(nn);
				n = nn;
			}

			// log.info("dot " + dot + " p:" + p.getName());

			Vector3f jmeVector = toJMEVector(n);
			buffers.ns.get(ia).set(jmeVector);
			buffers.ns.get(ib).set(jmeVector);
			buffers.ns.get(ic).set(jmeVector);
		}
	}

	private Material getMaterial(CTMaterial material) {
		return getDefaultMaterial(material.getColor());
	}

	private Material getDefaultMaterial() {
		Material m = new Material(getAssetManager(), "Common/MatDefs/Light/Lighting.j3md");
		// m.getAdditionalRenderState().setWireframe(true);

		return m;
	}

	private Material getDefaultMaterial(double[] color) {
		String srgb = getRGBString(color);
		Material m = materials.get(srgb);
		if (m == null) {
			m = getDefaultMaterial();
			m.setBoolean("UseMaterialColors", true);
			m.setColor("Diffuse", new ColorRGBA((float) color[0], (float) color[1], (float) color[2], 1f));
			m.setColor("Ambient", new ColorRGBA((float) color[0] / 2, (float) color[1] / 2, (float) color[2] / 2, 1f));
			materials.put(srgb, m);
		}

		return m;
	}

	private String getRGBString(double[] color) {
		int r = (int) (255 * color[0]);
		int g = (int) (255 * color[1]);
		int b = (int) (255 * color[2]);
		return "" + ((r << 16) + g << 8 + b);
	}

	private Material getHighlightMaterial() {
		Material m = new Material(getAssetManager(), "Common/MatDefs/Light/Lighting.j3md");
		m.setBoolean("UseMaterialColors", true);
		m.setColor("Diffuse", ColorRGBA.Yellow);
		return m;
	}

	private Vector3f toJMEVector(Vector3f v) {
		return new Vector3f((float) v.x, (float) v.y, (float) v.z);
	}

	@Override
	public synchronized void setPart(CTPart part) {
		if (currentpart == part) {
			return;
		}

		this.currentpart = part;

		log.info("set part " + part);

		startThread();
	}

	private synchronized void startThread() {
		if (processthread == null) {
			lastupdate = System.currentTimeMillis();

			processthread = new Thread(() -> {
				while (System.currentTimeMillis() - lastupdate < 4000) {
					loopStep();
				}
				processthread = null;
			}, "ObjectViewer setpart");
			processthread.start();
		}

		synchronized (checkflag) {
			checkflag.notify();
		}
	}

	private void loopStep() {
		CTPart processingpart = currentpart;
		String nodeid = getNodeID(processingpart);
		if (processingpart != null && !nodeid.equals(processednodeid)) {
			try {
				processednodeid = nodeid;
				Map<CTSubPart, SubpartMeshes> nsubpartmeshes = new HashMap<>();

				if (processingpart.getSubParts().isEmpty() && processingpart.getModel() != null
						&& processingpart.getModel().isReady()) {
					CTSubPartImpl defaultsubpart = new CTSubPartImpl(null, null);

					// should use part model
					defaultsubpart.setPart(processingpart);
					addSubPart(nsubpartmeshes, defaultsubpart);
				} else {
					processingpart.getSubParts().forEach(subpart -> {
						SubpartMeshes subpartmesh = nsubpartmeshes.get(subpart);
						if (subpartmesh == null) {
							subpartmesh = addSubPart(nsubpartmeshes, subpart);
						}
					});
				}

				subpartmeshes = nsubpartmeshes;
			} finally {
				processingpart = null;
				checkflag.trigger();
			}
		}

		synchronized (checkflag) {
			try {
				checkflag.wait(1000);
			} catch (InterruptedException e) {
				log.error(this, "processupdate", e);
				Thread.currentThread().interrupt();
			}
		}
	}

	private String getIdTree(CTPart processingpart) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setHighlight(Object o) {
		viewproperties.setHighlight(o);
	}

	@Override
	public void setSkip(int nskip) {
		this.viewproperties.setSkip(nskip);
	}

	private class Buffers {
		Geometry g;
		private List<Vector3f> vs = new ArrayList<>();
		private List<Vector3f> ns = new ArrayList<>();
		private List<Integer> tris = new ArrayList<>();

	}

	private class SubpartMeshes {
		Node n;
		Map<Material, Buffers> meshes = new HashMap<>();
	}
}
