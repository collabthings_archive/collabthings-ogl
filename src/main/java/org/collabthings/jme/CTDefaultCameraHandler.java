package org.collabthings.jme;

import java.util.LinkedList;
import java.util.List;

import org.collabthings.math.CTMath;
import org.collabthings.math.LOrientation;

import com.jme3.math.Vector3f;

public class CTDefaultCameraHandler implements CTCameraHandler {
	private static final int CAMERA_DISTANCE_MAX = 10000000;

	private Vector3f lookat;
	private Vector3f camlookat;
	private Vector3f camloc;
	private float cameradistance = 10000;

	private float mousey;
	private float cameradistancespeed = 10.0F;
	private float rotatex, camheight = 2;
	private boolean mousedown;

	private List<CTCamera> cams = new LinkedList<>();

	@Override
	public void addCamera(CTCamera cam) {
		cams.add(cam);
	}

	public void setLookAt(Vector3f location) {
		this.lookat = location;
	}

	public void update(float tpf) {
		updateValues(tpf);
		updateCam(tpf);
	}

	public void updateCam(float dtime) {
		if (camlookat == null) {
			camlookat = new Vector3f();
		}

		if (camloc == null) {
			camloc = new Vector3f();
		}

		if (lookat == null) {
			lookat = new Vector3f();
		}

		Vector3f distance = lookat.subtract(camlookat);
		float dlookatdistance = dtime * 0.70f;
		if (dlookatdistance > 1) {
			dlookatdistance = 1;
		}

		distance.multLocal(dlookatdistance);

		camlookat.addLocal(distance);
		// camlookat.set(lookat);

		Vector3f camv = new Vector3f(0, camheight, -1);
		camv = new LOrientation(new Vector3f(0, 1, 0), rotatex).getTransformation().transformVector(camv,
				new Vector3f());

		camv = camv.normalize().mult(cameradistance);

		Vector3f camlocwanted = camv.add(camlookat);
		float dcamlocdistance = dtime * 2.7000f;
		if (dcamlocdistance > 1) {
			dcamlocdistance = 1;
		}

		Vector3f camlocdistance = camlocwanted.subtract(camloc).mult(dcamlocdistance);
		camloc.addLocal(camlocdistance);

		for (CTCamera cam : cams) {
			cam.setDistance(cameradistance);
			cam.setLocation(camloc.clone());
			cam.lookAt(camlookat, new Vector3f(0, 1, 0));
		}
	}

	private void updateValues(float tpf) {
		camheight -= mousey * 5000f * tpf;

		rotatex = CTMath.limitAngle(rotatex);

		if (camheight > 4) {
			camheight = 4;
		} else if (camheight < -4) {
			camheight = -4;
		}

		double dmult = 0.95 - tpf * 100;
		if (dmult < 0) {
			dmult = 0;
		}

		mousey *= dmult;

		if (cameradistancespeed < 0.5) {
			cameradistancespeed = 0.5f;
		} else if (cameradistancespeed > 1.5) {
			cameradistancespeed = 1.5f;
		}

		cameradistance *= cameradistancespeed;
		if (cameradistance < 10) {
			cameradistance = 10;
		} else if (cameradistance > CAMERA_DISTANCE_MAX) {
			cameradistance = CAMERA_DISTANCE_MAX;
		}

		float zoommult = 1.0F - tpf * 100;
		if (zoommult < 0) {
			zoommult = 0;
		}

		cameradistancespeed = ((cameradistancespeed - 1) * zoommult) + 1;
	}

	public void mouseDown(boolean arg1) {
		this.mousedown = arg1;
	}

	public void updateMouseZoom(float zoomvalue) {
		cameradistancespeed += zoomvalue * 1.0F;
	}

	public void updateMouseX(float value) {
		if (mousedown) {
			rotatex += -value * 40.0F;
		}
	}

	public void updateMouseY(float value) {
		if (mousedown) {
			mousey = value * 0.1f;
		}
	}
}
