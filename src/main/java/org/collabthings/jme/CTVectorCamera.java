package org.collabthings.jme;

import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;

public class CTVectorCamera implements CTCamera {

	private float distance;
	private Vector3f camlookat;
	private Camera cam;
	private Vector3f v;

	public CTVectorCamera(Camera cam2, Vector3f vector3f) {
		this.cam = cam2;
		this.v = vector3f;
	}

	private void calculate() {
		if (distance > 0.1f && camlookat != null) {
			Vector3f location = camlookat.subtract(v.mult(distance));
			cam.setLocation(location);
			Vector3f up = new Vector3f(0, 1, 0);
			if (v.dot(up) > 0.9f) {
				up = new Vector3f(1, 0, 0);
			}

			cam.lookAt(camlookat, up);
		}
	}

	@Override
	public void setDistance(float cameradistance) {
		this.distance = cameradistance;
		calculate();
	}

	@Override
	public void setLocation(Vector3f clone) {
		// nothing to do
	}

	@Override
	public void lookAt(Vector3f camlookat, Vector3f vector3f) {
		this.camlookat = camlookat;
		calculate();
	}

}
