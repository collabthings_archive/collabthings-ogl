package org.collabthings.jme;

import org.collabthings.model.CTPart;

import com.jme3.scene.Node;

public interface CTObjectViewer {

	void setPart(CTPart part);

	void setSkip(int nskip);

	void setHighlight(Object o);

	void update(Node pivot);

	void setScale(float f);

	float getScale();

}
