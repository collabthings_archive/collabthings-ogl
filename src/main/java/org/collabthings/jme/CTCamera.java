package org.collabthings.jme;

import com.jme3.math.Vector3f;

public interface CTCamera {

	void setDistance(float cameradistance);

	void setLocation(Vector3f clone);

	void lookAt(Vector3f camlookat, Vector3f vector3f);

}
