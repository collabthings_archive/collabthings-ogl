package org.collabthings.jme;

import com.jme3.math.Vector3f;

public interface CTCameraHandler {

	void updateMouseX(float value);

	void updateMouseY(float value);

	void mouseDown(boolean arg1);

	void update(float tpf);

	void setLookAt(Vector3f mult);

	void updateMouseZoom(float f);

	void addCamera(CTCamera cam);

}
