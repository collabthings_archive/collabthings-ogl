package org.collabthings.jme;

import java.awt.Canvas;
import java.awt.Dimension;

import org.collabthings.model.CTPart;
import org.collabthings.model.CTSubPart;
import org.collabthings.util.LLog;
import org.lwjgl.input.Mouse;

import com.jme3.app.SimpleApplication;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;

public class CTSceneApp extends SimpleApplication {
	private CTObjectViewer scene;
	private Node pivot;

	private LLog log = LLog.getLogger(this);

	private CTCameraHandler camhandler;

	public void setScene(CTObjectViewer scene) {
		this.scene = scene;
	}

	public void init() {
		showSettings = false;

		AppSettings settings = new AppSettings(true);
		settings.setWidth(640);
		settings.setHeight(480);
		settings.setVSync(true);

		settings.setSamples(4);
		settings.setFrameRate(17);
		log.info("depthbits " + settings.getDepthBits());

		setSettings(settings);

		setPauseOnLostFocus(false);

		createCanvas();
		startCanvas();
	}

	public Canvas getCanvas(int w, int h) {
		JmeCanvasContext ctx = (JmeCanvasContext) getContext();
		ctx.setSystemListener(this);
		Dimension dim = new Dimension(w, h);
		ctx.getCanvas().setPreferredSize(dim);
		return ctx.getCanvas();
	}

	@Override
	public void simpleInitApp() {
		/** create a blue box at coordinates (1,-1,1) */
		// Box firstobject = new Box(1, 1, 1);
		Sphere firstobject = new Sphere(8, 8, 1);
		Geometry blue = new Geometry("Box", firstobject);
		blue.setLocalTranslation(new Vector3f(0, 0, 0));
		Material mat1 = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
		// mat1.setColor("Color", ColorRGBA.Blue);
		blue.setMaterial(mat1);

		pivot = new Node("pivot");

		rootNode.attachChild(pivot); // put this node in the scene

		AmbientLight ambient = new AmbientLight();
		// ambient.setColor(ColorRGBA.LightGray);
		rootNode.addLight(ambient);

		DirectionalLight sun = new DirectionalLight();
		sun.setColor(ColorRGBA.White);
		sun.setDirection(new Vector3f(0, -1, 1).normalizeLocal());
		rootNode.addLight(sun);

		DirectionalLight sun2 = new DirectionalLight();
		sun2.setColor(ColorRGBA.LightGray);
		sun2.setDirection(new Vector3f(-1, 1, 1).normalizeLocal());
		rootNode.addLight(sun2);

		pivot.attachChild(blue);

		setDisplayStatView(true);

		inputManager.setCursorVisible(true);
		Mouse.setGrabbed(false);
		flyCam.setEnabled(false);

		setDisplayFps(true);
		setDisplayStatView(true);
		guiViewPort.setEnabled(true);
		cam.setFrustumFar(1000 * 1000 * 1000);
		cam.setViewPort(.5f, 1f, 0f, 0.5f);

		Camera cam2 = cam.clone();
		cam2.setViewPort(.0f, 0.5f, 0f, 0.5f);
		ViewPort view2 = renderManager.createMainView("Bottom Left", cam2);
		view2.setClearFlags(true, true, true);
		view2.attachScene(rootNode);

		Camera cam3 = cam.clone();
		cam3.setViewPort(.0f, 0.5f, 0.5f, 1.0f);
		ViewPort view3 = renderManager.createMainView("Top Left", cam3);
		view3.setClearFlags(true, true, true);
		view3.attachScene(rootNode);

		Camera cam4 = cam.clone();
		cam4.setViewPort(.5f, 1.0f, 0.5f, 1.0f);
		ViewPort view4 = renderManager.createMainView("Top Right", cam4);
		view4.setClearFlags(true, true, true);
		view4.attachScene(rootNode);

		camhandler = new CTDefaultCameraHandler();
		camhandler.addCamera(new CTFlyCamera(cam));
		camhandler.addCamera(new CTVectorCamera(cam2, new Vector3f(0, -1, 0)));
		camhandler.addCamera(new CTVectorCamera(cam3, new Vector3f(1, 0, 0)));
		camhandler.addCamera(new CTVectorCamera(cam4, new Vector3f(0, 0, 1)));

		inputManager.addMapping("Button1", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
		inputManager.addMapping("MoveX", new MouseAxisTrigger(MouseInput.AXIS_X, false));
		inputManager.addMapping("MoveY", new MouseAxisTrigger(MouseInput.AXIS_Y, false));
		inputManager.addMapping("MoveXn", new MouseAxisTrigger(MouseInput.AXIS_X, true));
		inputManager.addMapping("MoveYn", new MouseAxisTrigger(MouseInput.AXIS_Y, true));
		inputManager.addMapping("Wheel", new MouseAxisTrigger(MouseInput.AXIS_WHEEL, false));
		inputManager.addMapping("Wheeln", new MouseAxisTrigger(MouseInput.AXIS_WHEEL, true));

		inputManager.addListener(new ActionListener() {
			@Override
			public void onAction(String arg0, boolean arg1, float arg2) {
				camhandler.mouseDown(arg1);
			}
		}, "Button1");

		AnalogListener analogListener = new AnalogListener() {

			@Override
			public void onAnalog(String name, float value, float tpf) {
				if (name.contains("n")) {
					value *= -1;
				}

				if (name.contains("Move")) {
					if (name.contains("X")) {
						camhandler.updateMouseX(value);
					} else if (name.contains("Y")) {
						camhandler.updateMouseY(value);
					}
				} else if (name.contains("Wheel")) {
					camhandler.updateMouseZoom(-value / 10.0F);
				}
			}

		};

		inputManager.addListener(analogListener, "MoveX");
		inputManager.addListener(analogListener, "MoveXn");
		inputManager.addListener(analogListener, "MoveY");
		inputManager.addListener(analogListener, "MoveYn");
		inputManager.addListener(analogListener, "Wheeln");
		inputManager.addListener(analogListener, "Wheel");
	}

	public void setBackgroundColor(float r, float g, float b) {
		if (viewPort != null) {
			viewPort.setBackgroundColor(new ColorRGBA(r, g, b, 1));
		}
	}

	@Override
	public void simpleUpdate(float tpf) {
		if (tpf > 0.1f) {
			tpf = 0.1f;
		}

		try {
			if (scene != null && pivot != null) {
				camhandler.update(tpf);

				scene.update(pivot);
			}
		} catch (Exception e) {
			log.info("ERROR " + e);
			log.error(this, "simpleUpdate", e);
		}
	}

	@Override
	public void update() {
		try {
			super.update();
		} catch (Exception e) {
			log.info("ERRRO " + e);
			log.error(this, "update", e);
		}
	}

	public static void main(String[] args) {
		CTSceneApp app = new CTSceneApp();
		app.start();
	}

	public void close() {
		stop();
	}

	public CTObjectViewer newScene() {
		return new CTObjectViewerImpl(this.getAssetManager());
	}

	public boolean isReady() {
		return assetManager != null && viewPort != null;
	}

	public void setHighlight(Object o) {
		if (o instanceof CTSubPart) {
			CTSubPart subpart = (CTSubPart) o;
			CTPart part = subpart.getPart();

			Vector3f location = subpart.getLocation().clone();
			if (part != null) {
				location.addLocal(
						subpart.getTransformation().getRotation().mult(part.getViewingProperties().getLookAt()));
			}

			setLookAt(location);
		}
	}

	public void setLookAt(Vector3f vector3f) {
		camhandler.setLookAt(vector3f.mult(scene.getScale()));
	}

}
