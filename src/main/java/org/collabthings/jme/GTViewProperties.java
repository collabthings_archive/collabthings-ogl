package org.collabthings.jme;

import java.util.Stack;

public class GTViewProperties {

	private double explode;
	private int skip;
	private Object highlight;

	private Stack<float[]> overrideColor = new Stack<>();
	private boolean highlighton;

	public double getExplode() {
		return this.explode;
	}

	public int getSkip() {
		return skip;
	}

	public void setSkip(int i) {
		this.skip = i;
	}

	public Object getHighlight() {
		return highlight;
	}

	public void setHighlight(Object highlight) {
		this.highlight = highlight;
	}

	public float[] getOverrideColor() {
		if (overrideColor.isEmpty()) {
			return null;
		} else {
			return overrideColor.peek();
		}
	}

	public void pushOverrideColor(float[] highlightColor) {
		overrideColor.push(highlightColor);
	}

	public void popOverrideColor() {
		overrideColor.pop();
	}

	public void setHighlightOn(boolean b) {
		this.highlighton = b;
	}

	public boolean isHighlightOn() {
		return highlighton;
	}

}
